var Genre = require('../models/genre');
var Book = require('../models/book');

// Display list of all Genre.
exports.genre_list = function(req, res, next) {

    Genre.find()
        .sort([['name', 'ascending']]) // Tri par nom
        .exec(function (err, list_genres) {
            if (err) { return next(err); }
            res.render('genre_list', { title: 'Liste des genres', list_genres:  list_genres});
        });

};

// Affiche la page de détail pour un genre.
exports.genre_detail = function(req, res, next) {

    Promise.all([
        new Promise(function(resolve, reject) { // genre
            Genre.findById(req.params.id).exec(function(err, genre) {
                if (err) {
                    reject(err);
                }
                resolve(genre);
            });
        }),
        new Promise(function(resolve, reject) { // genre_books
            Book.find({ 'genre': req.params.id }).exec(function(err, genre_books) {
                if (err) {
                    reject(err);
                }
                resolve(genre_books);
            });
        })
    ]).then(function(results) {
        
        if (results[0] == null) { // Pas de résultat => Erreur 404
            var err = new Error('Genre introuvable');
            err.status = 404;
            return next(err);
        }

        res.render('genre_detail', {
            title: "Détail d'un genre", 
            genre: results[0], 
            genre_books: results[1] 
        });

    }).catch(function(err) {
        next(err);
    });

};

// Display Genre create form on GET.
exports.genre_create_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre create GET');
};

// Handle Genre create on POST.
exports.genre_create_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre create POST');
};

// Display Genre delete form on GET.
exports.genre_delete_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre delete GET');
};

// Handle Genre delete on POST.
exports.genre_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre delete POST');
};

// Display Genre update form on GET.
exports.genre_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre update GET');
};

// Handle Genre update on POST.
exports.genre_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre update POST');
};